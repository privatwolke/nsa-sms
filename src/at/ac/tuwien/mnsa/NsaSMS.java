package at.ac.tuwien.mnsa;

import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import at.ac.tuwien.mnsa.connection.USBConnectionFactory;
import at.ac.tuwien.mnsa.connection.USBConnectionForModem;
import at.ac.tuwien.mnsa.sms.SMS;
import at.ac.tuwien.mnsa.sms.SMS.MessageClass;

import com.googlecode.jcsv.reader.CSVReader;
import com.googlecode.jcsv.reader.internal.CSVReaderBuilder;

public class NsaSMS {

	private static Logger logger = Logger.getLogger(NsaSMS.class);

	public static void main(String[] args) throws Exception {
		BasicConfigurator.configure();

		verifyArguments(args);

		USBConnectionForModem modemConnection = USBConnectionFactory.getConnection();
		if (modemConnection == null) {
			logger.info("USB Modem was not found.");
			System.exit(2);
		}

		CSVReader<String[]> csvReader = CSVReaderBuilder.newDefaultReader(new FileReader(args[0]));

		try {
			for (String[] line : csvReader.readAll()) {
				assert line[0].matches("\\+43[0-9]+");

				if (line.length == 2) {
					modemConnection.transmit(new SMS(line[0], line[1]));
				} else if (line.length == 5) {
					String recipient = line[0];
					String message = line[1];
					boolean flash = Boolean.parseBoolean(line[2]);
					Date validity = new SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy").parse(line[3]);
					boolean receipt = Boolean.parseBoolean(line[4]);

					modemConnection.transmit(new SMS(
						null, recipient, message, flash ? MessageClass.FLASH : MessageClass.NONE, receipt, validity)
					);
				}
			}
		} catch (ParseException | AssertionError | IndexOutOfBoundsException e) {
			logger.error("Read CSV line of unexpected format.");
		} finally {
			modemConnection.disconnect();
		}

		logger.info("Finished sending of SMS.");
	}

	private static void verifyArguments(String[] args) {
		if (args.length != 1) {
			logger.info("Usage: java NsaSMS input.csv");
			System.exit(1);
		}
	}

}
