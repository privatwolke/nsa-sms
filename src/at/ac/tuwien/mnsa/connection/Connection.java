package at.ac.tuwien.mnsa.connection;

import java.io.IOException;

import at.ac.tuwien.mnsa.sms.SMS;

/**
 * Defines the basic methods required to communicate with a USB Modem.
 */
public interface Connection {

	/**
	 * @return true if connection is active, false if not.
	 */
	boolean isActive();

	/**
	 * transmit a sms
	 * 
	 * @param msisdn number to send the message to
	 * @param message the message to send
	 * @return Returns true on success
	 */
	void transmit(SMS sms) throws IOException;
	
	/**
	 * transmit an AT-command
	 * 
	 * @param msisdn number to send the message to
	 * @param message the message to send
	 * @return Returns the response of the modem
	 */
	String transmit(String atCommand, int lengthOfResult) throws IOException;	
	
	/**
	 * Closes the port of the connection.
	 */
	void disconnect();

}
