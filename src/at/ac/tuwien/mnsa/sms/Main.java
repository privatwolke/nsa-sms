package at.ac.tuwien.mnsa.sms;

public class Main {

	public static void main(String[] args) throws Exception {
		SMS s = new SMS(
			"+436604702755",
			"However, if the GSM/GPRS modem or mobile phone is operating in SMS PDU mode., executing the above command line will cause an error to occur. This is because the syntax of the +CMGS AT command is different in SMS PDU mode. To do the same task, the following command line should be used instead. bla");
		for (byte[] sms : s.getBytes()) {
			System.out.println(bytesToHex(sms));
		}
	}

	public static String bytesToBin(byte[] bytes) {
		StringBuffer sb = new StringBuffer();
		for (byte b : bytes) {
			StringBuilder sbb = new StringBuilder("00000000");
			for (int bit = 0; bit < 8; bit++) {
				if (((b >> bit) & 1) > 0) {
					sbb.setCharAt(bit, '1');
				}
			}

			sb.append(sbb.toString());
			sb.append(" ");
		}
		return sb.toString().trim();
	}

	public static String bytesToHex(byte[] bytes) {
		final char[] hexArray = {
			'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
		};
		char[] hexChars = new char[bytes.length * 2];
		int v;
		for (int j = 0; j < bytes.length; j++) {
			v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

}
