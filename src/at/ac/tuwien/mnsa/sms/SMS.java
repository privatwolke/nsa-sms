package at.ac.tuwien.mnsa.sms;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import at.ac.tuwien.mnsa.sms.SMSUserData.SMSUserDataPart;

public class SMS {

	public static enum MessageClass {
		NONE("00"), FLASH("00"), ME_SPECIFIC("01"), SIM_SPECIFIC("10"), TE_SPECIFIC("11");

		private String value;

		private MessageClass(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return this.value;
		}
	}

	private class GeneralDataCodingScheme {

		// holds the DCS value
		private byte value;

		public GeneralDataCodingScheme(boolean compressedText, boolean defaultAlphabet,
			MessageClass messageClass) {
			this.value = (byte) Integer.parseInt("00" + (compressedText ? "1" : "0")
				+ (messageClass != MessageClass.NONE ? "1" : "0") + (defaultAlphabet ? "00" : "10")
				+ messageClass.toString(), 2);
		}

		public byte getValue() {
			return this.value;
		}

	}

	public static final String SMSC_DREI = "+4366000660";

	private String smsc = null, receiver;
	private SMSUserData userData;
	private MessageClass messageClass = MessageClass.NONE;
	private boolean receipt = false;
	private Date validity = null;

	public SMS(String receiver, String message) {
		this.setReceiver(receiver);
		this.setUserData(new SMSUserData(message));
	}

	public SMS(String smsc, String receiver, String message, MessageClass messageClass,
		boolean receipt, Date validity) {
		this.setSMSC(smsc);
		this.setReceiver(receiver);
		this.setUserData(new SMSUserData(message));
		this.messageClass = messageClass;
		this.receipt = receipt;
		this.validity = validity;
	}

	public List<byte[]> getBytes() {

		byte lenReceiver;
		byte[] receiver;
		List<byte[]> messages = new ArrayList<byte[]>();
		List<SMSUserDataPart> parts = this.getUserData().getParts();

		receiver = stringToSemiOctets(this.getReceiver());
		lenReceiver = (byte) (this.getReceiver().length() - (this.getReceiver().endsWith("F") ? 1 : 0));

		for (SMSUserDataPart message : parts) {

			ByteBuffer pdu = ByteBuffer.allocate(200);
			byte lenMessage = message.getLength();

			// SMSC
			pdu.put(this.getHeader());

			// SMS-SUBMIT type byte
			pdu.put((byte) Integer.parseInt("0" // no reply path
				+ (parts.size() > 1 ? "1" : "0") // if more than one part, a user data header is present
				+ (this.receipt ? "1" : "0") // if receipt is requested
				+ (this.validity != null ? "11" : "00") // if validity field is present
				+ "001" // do not reject duplicates, PDU is of type SMS-SUBMIT
			, 2));

			// reference number
			pdu.put((byte) new Random().nextInt(255));

			// receiver number length
			pdu.put(lenReceiver);

			// receiver address type (international)
			pdu.put((byte) 0x91);

			// receiver address
			pdu.put(receiver);

			// protocol identifier
			pdu.put((byte) 0x00);

			// data coding scheme
			GeneralDataCodingScheme gds = new GeneralDataCodingScheme(false, message.is7BitEncoded(),
				this.messageClass);
			pdu.put(gds.getValue());

			// validity period
			if (this.validity != null) {
				pdu.put(getSemiOctetTimestamp(this.validity));
			}

			// user data length
			pdu.put(lenMessage);

			// user data
			pdu.put(message.getData());

			messages.add(Arrays.copyOfRange(pdu.array(), 0, pdu.position()));
		}

		return messages;
	}

	private byte[] getHeader() {
		byte[] SMSC = (this.getSMSC() != null) ? stringToSemiOctets((this.getSMSC())) : null;
		byte lenHeader = (byte) ((SMSC == null) ? 0 : (SMSC.length + 1));

		ByteBuffer header = ByteBuffer.allocate(100);

		// header length
		header.put(lenHeader);

		// non zero header length means that we specify a SMSC address
		if (this.getSMSC() != null) {

			// SMSC address type (international)
			header.put((byte) 0x91);

			// SMSC address in semi-octet encoding
			header.put(SMSC);
		}

		return Arrays.copyOfRange(header.array(), 0, header.position());
	}

	public int getHeaderLength() {
		return this.getHeader().length;
	}

	public void setSMSC(String smscNumber) {

		// null value means that SMS use the SMSC from the phone
		if (smscNumber == null) {
			this.smsc = null;
			return;
		}

		if (!smscNumber.matches("\\+[0-9]{2,}"))
			throw new IllegalArgumentException(
				"Invalid SMSC. Use international format (e.g. +436641234).");

		this.smsc = smscNumber.substring(1);
		if (this.smsc.length() % 2 != 0)
			this.smsc += "F";
	}

	public String getSMSC() {
		return this.smsc;
	}

	public void setReceiver(String receiverNumber) {
		if (!receiverNumber.matches("\\+[0-9]{2,}"))
			throw new IllegalArgumentException(
				"Invalid Receiver. Use international format (e.g. +436641234).");

		this.receiver = receiverNumber.substring(1);
		if (this.receiver.length() % 2 != 0)
			this.receiver += "F";
	}

	public String getReceiver() {
		return this.receiver;
	}

	public void setUserData(SMSUserData message) {
		this.userData = message;
	}

	private SMSUserData getUserData() {
		return this.userData;
	}

	protected static byte[] stringToSemiOctets(String input) {
		byte[] out = new byte[input.length() / 2];
		for (int i = 0, j = 0; i < input.length(); i = i + 2, j++) {
			String temp = input.substring(i, i + 2);
			out[j] = (byte) Integer.parseInt("" + temp.charAt(1) + temp.charAt(0), 16);
		}
		return out;
	}

	protected static byte[] getSemiOctetTimestamp(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		byte[] out = new byte[7];

		out[0] = Integer.valueOf(
			new StringBuffer("" + cal.get(Calendar.YEAR)).reverse().substring(0, 2).toString(), 16)
			.byteValue();
		out[1] = getReverseOctet(cal.get(Calendar.MONTH) + 1);
		out[2] = getReverseOctet(cal.get(Calendar.DAY_OF_MONTH));
		out[3] = getReverseOctet(cal.get(Calendar.HOUR_OF_DAY));
		out[4] = getReverseOctet(cal.get(Calendar.MINUTE));
		out[5] = getReverseOctet(cal.get(Calendar.SECOND));
		out[6] = Integer.valueOf((byte) (cal.getTimeZone().getRawOffset() / 1000 * 4)).byteValue();

		return out;
	}

	private static byte getReverseOctet(int value) {
		return Integer.valueOf(new StringBuffer(String.format("%02d", value)).reverse().toString(), 16)
			.byteValue();
	}

}
