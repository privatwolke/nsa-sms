package at.ac.tuwien.mnsa.sms;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Holds SMS User Data and deals with encoding it correctly, reporting its length and dealing with
 * long SMS.
 * 
 * @author stephan
 * 
 */
public class SMSUserData {

	private final static char[] GSM_ALPHABET = new char[] {
		'\u0040', '\u00A3', '\u0024', '\u00A5', '\u00E8', '\u00E9', '\u00F9', '\u00EC', '\u00F2',
		'\u00C7', '\n', '\u00D8', '\u00F8', '\r', '\u00C5', '\u00E5', '\u0394', '\u005F', '\u03a6',
		'\u0393', '\u039b', '\u03a9', '\u03a0', '\u03a8', '\u03a3', '\u0398', '\u039e', '\u261d',
		'\u00C6', '\u00E6', '\u00DF', '\u00C9', '\u0020', '\u0021', '\u0022', '\u0023', '\u00A4',
		'\u0025', '\u0026', '\'', '\u0028', '\u0029', '\u002A', '\u002B', '\u002C', '\u002D', '\u002E',
		'\u002F', '\u0030', '\u0031', '\u0032', '\u0033', '\u0034', '\u0035', '\u0036', '\u0037',
		'\u0038', '\u0039', '\u003A', '\u003B', '\u003C', '\u003D', '\u003E', '\u003F', '\u00A1',
		'\u0041', '\u0042', '\u0043', '\u0044', '\u0045', '\u0046', '\u0047', '\u0048', '\u0049',
		'\u004A', '\u004B', '\u004C', '\u004D', '\u004E', '\u004F', '\u0050', '\u0051', '\u0052',
		'\u0053', '\u0054', '\u0055', '\u0056', '\u0057', '\u0058', '\u0059', '\u005A', '\u00C4',
		'\u00D4', '\u00D1', '\u00DC', '\u00A7', '\u00BF', '\u0061', '\u0062', '\u0063', '\u0064',
		'\u0065', '\u0066', '\u0067', '\u0068', '\u0069', '\u006A', '\u006B', '\u006C', '\u006D',
		'\u006E', '\u006F', '\u0070', '\u0071', '\u0072', '\u0073', '\u0074', '\u0075', '\u0076',
		'\u0077', '\u0078', '\u0079', '\u007A', '\u00E4', '\u00F6', '\u00F1', '\u00FC', '\u00E0', '\f',
		'\u005E', '\u007B', '\u007D', '\u002F', '\u005B', '\u007E', '\u005D', '\u007C', '\u20AC',
	};

	/**
	 * Represents a UserData field in an SMS PDU for long SMS.
	 * 
	 * @author stephan
	 * 
	 */
	protected class SMSUserDataPart {

		private byte[] data;
		private byte length;
		private boolean is7BitEncoded;

		public SMSUserDataPart(byte[] data, byte length, boolean is7BitEncoded) {
			this.data = data;
			this.length = length;
			this.is7BitEncoded = is7BitEncoded;
		}

		public SMSUserDataPart(byte[] data, boolean is7BitEncoded) {
			this(data, (byte) data.length, is7BitEncoded);
		}

		public byte[] getData() {
			return this.data;
		}

		public byte getLength() {
			return this.length;
		}

		public boolean is7BitEncoded() {
			return this.is7BitEncoded;
		}

	}

	private String message;

	public SMSUserData(String message) {
		this.message = message;
	}

	/**
	 * @return the raw message
	 */
	private String getMessage() {
		return this.message;
	}

	/**
	 * Encodes the message dependent on its content characters. If the message exceeds maximum
	 * characters, it will be split. The returned SMSUserDataPart instances may have different
	 * encodings based on their contents.
	 * 
	 * @return a list of SMSUserDataPart instances
	 */
	public List<SMSUserDataPart> getParts() {
		String message;
		byte[] udh = new byte[] {};
		List<SMSUserDataPart> results = new ArrayList<SMSUserDataPart>();
		String[] splits = SMSUserData.getSplits(this.getMessage());

		int referenceNumber = new Random().nextInt(0xFF);

		for (int i = 0, totalMessages = splits.length; i < totalMessages; i++) {

			byte[] messageBytes, userData;
			message = splits[i];

			messageBytes = message.getBytes();

			if (totalMessages > 1) {
				// more than one message to send, create user data header
				udh = new byte[] {
					// user data header is 5 byte long
					(byte) 0x05,
					// concatenated sms, 8 bit reference number
					(byte) 0x00,
					// user data header content is 3 byte long
					(byte) 0x03,
					// reference number
					(byte) referenceNumber,
					// total messages (00 - FF)
					(byte) totalMessages,
					// number of this message
					(byte) (i + 1),
				// padding byte to align to 7 bit boundaries
				// (byte) 0x00
				};
			}

			// getSplits has already ensured that messages > 70 are GSM-7 compatible
			if (udh.length + splits[0].length() > 70) {
				try {
					// if a shift will cause one bit to overflow, padd the message with a carriage return
					// this will not cause the message to exceed 160 characters, because
					// (160 + 2) % 8 = 2 and (159 + 2) % 8 = 1
					if ((udh.length + message.length() + 2) % 8 == 0) {
						message += "\r";
					}

					messageBytes = shiftRight(message.getBytes("SCPGSM"), totalMessages > 1 ? 1 : 0);
					userData = new byte[udh.length + messageBytes.length];
					System.arraycopy(udh, 0, userData, 0, udh.length);
					System.arraycopy(messageBytes, 0, userData, udh.length, messageBytes.length);
					
					// length calculation
					byte septets = (byte) ((messageBytes.length * 8) / 7);

					results.add(new SMSUserDataPart(userData, (byte) (udh.length + septets), true));
				} catch (UnsupportedEncodingException e) {
					return null;
				}
			} else {
				messageBytes = UCS2Mapper.encode(message);
				userData = new byte[udh.length + messageBytes.length];
				System.arraycopy(udh, 0, userData, 0, udh.length);
				System.arraycopy(messageBytes, 0, userData, udh.length, messageBytes.length);
				results.add(new SMSUserDataPart(userData, false));
			}
		}

		return results;
	}

	/**
	 * Shifts a byte array right by n steps. Input is in "wrong" byte order.
	 * 
	 * Before: 00010111 00001110
	 * _After: 00001011 10000111
	 * 
	 * @param input
	 * @param n
	 * @return the shifted array
	 */
	public static byte[] shiftRight(byte[] input, int n) {
		
		if (n == 0) return input;
		
		assert n < 8;

		int lsb = 0;
		ByteBuffer buf = ByteBuffer.allocate(input.length + 1);
		int lsbMask = 0xFF - (int) Math.max(1, Math.pow(2, n) - 1);

		for (int i = 0, j = input.length; i < j; i++) {
			// store the least significant bits
			int newlsb = input[i] & lsbMask;

			// shift value left by n steps
			input[i] = (byte) (input[i] << n);

			// set the n most significant bytes to zero, then set them to the previous bytes' lsbs
			buf.put((byte) ((input[i] & ~(lsbMask >> (8 - n))) | (lsb >> (8 - n))));
			lsb = newlsb;
		}

		return Arrays.copyOfRange(buf.array(), 0, buf.position());
	}

	/**
	 * checks whether the input can be encoded with the characters present in the GSM 03.38 charset.
	 * 
	 * @param input
	 * @return the position of the first character that is not compatible, if all characters are
	 *         compatible return -1
	 */
	protected static int is7BitCompatible(String input) {
		for (int i = 0, j = input.length(); i < j; i++) {
			boolean validChar = false;
			for (int k = 0, l = SMSUserData.GSM_ALPHABET.length; k < l; k++) {
				if (SMSUserData.GSM_ALPHABET[k] == input.charAt(i)) {
					validChar = true;
					break;
				}
			}
			if (!validChar) {
				return i;
			}
		}

		return -1;
	}

	/**
	 * Splits input into chunks of 153 or 63 characters, depending on the encoding compatibility.
	 * 
	 * @param message
	 * @return
	 */
	protected static String[] getSplits(String message) {
		if (is7BitCompatible(message) == -1) {
			if (message.length() <= 160) {
				return new String[] {
					message
				};
			}

			return message.split("(?<=\\G.{153})");
		} else {
			if (message.length() <= 70) {
				return new String[] {
					message
				};
			}
			return message.split("(?<=\\G.{63})");
		}
	}

}
