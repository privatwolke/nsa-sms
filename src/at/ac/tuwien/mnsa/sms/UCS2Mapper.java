package at.ac.tuwien.mnsa.sms;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class UCS2Mapper {

	public static byte[] encode(String input) {
		byte[] bytes;
		try {
			bytes = input.getBytes("UTF-16");
		} catch (UnsupportedEncodingException e) {
			bytes = new byte[] {
				(byte) 0xFE, (byte) 0xFF
			};
		}
		return Arrays.copyOfRange(bytes, 2, bytes.length);
	}

	public static String decode(byte[] input) {
		try {
			return new String(input, "UTF-16");
		} catch (UnsupportedEncodingException e) {
			return "";
		}
	}
}
